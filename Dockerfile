FROM rclone/rclone AS builder

# Begin final image
FROM python:3

RUN echo 'export XDG_CONFIG_HOME=/data/.config' >> /etc/profile; \
  chmod 777 /data; \
  apt update && apt install -y ca-certificates fuse tzdata && \
  echo "user_allow_other" >> /etc/fuse.conf

WORKDIR /data

COPY --from=builder /usr/local/bin/rclone /usr/local/bin/

ADD https://github.com/cjnaz/rclonesync-V2/blob/master/rclonesync /usr/local/bin/

# COPY ./entrypoint.sh /entrypoint.sh

# ENTRYPOINT [ "/entrypoint.sh" ]

CMD [ 'rclonesync', '--workdir', '/data' ]
